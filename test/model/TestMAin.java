package model;

import main.model.Game;
import main.model.Hunter;
import main.model.Maze;
import main.model.Monster;

public class TestMAin {

    public static void main(String[] args) {
        Game g = new Game();
        g.maze = new Maze();
        g.hunter = new Hunter(g.maze);
        g.monster = new Monster(g.maze);
        System.out.println(g.maze);
        g.play();
    }
}
