package model;

import main.model.Cell;
import main.model.TypeCell;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class testCell {
    Cell c;

    @Before
    public void setup() {
        c = new Cell();
    }

    @Test
    public void TestCreationCell() {
        assertFalse(c.hasBeenShot);
        assertEquals(-1, c.getTurn());
        assertEquals(TypeCell.UNKNOWN, c.type);
    }

    @Test
    public void TestModificationCell() {

        assertFalse(c.hasBeenShot);
        assertEquals(-1, c.getTurn());
        assertEquals(TypeCell.UNKNOWN, c.type);
        c.type = TypeCell.EXIT;
        assertNotEquals(TypeCell.UNKNOWN, c.type);
        assertEquals(TypeCell.EXIT, c.type);
        c.hasBeenShot = true;
        assertTrue(c.hasBeenShot);
        c.setTurn(5);
        assertNotEquals(0, c.getTurn());
        assertEquals(5, c.getTurn());
    }
}
