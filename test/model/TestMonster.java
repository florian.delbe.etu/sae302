package model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import main.model.Coordinate;
import main.model.Maze;
import main.model.Monster;
import main.model.TypeCell;

public class TestMonster {

    Monster monster;

    @Before
    public void setup() {
        monster = new Monster(new Coordinate(0, 0));
    }

    @Test
    public void testMove() {
        Maze m = new Maze(7, 7);
        m.map[0][1].type = TypeCell.WALL;
        m.map[1][0].type = TypeCell.PATH;
        m.map[1][1].type = TypeCell.EXIT;
        m.map[2][0].type = TypeCell.VOID;
        assertTrue(monster.move(new Coordinate(1, 0), m));// test for Path
        assertFalse(monster.move(new Coordinate(0, 1), m));// test for Wall
        assertFalse(monster.move(new Coordinate(5, 1), m));// test for Unknow
        assertFalse(monster.move(new Coordinate(2, 0), m));// test for Void
        assertTrue(monster.move(new Coordinate(1, 1), m));// test for Exit
    }
}
