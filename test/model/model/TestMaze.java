package model;

import main.model.Coordinate;
import main.model.Maze;
import main.model.TypeCell;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestMaze {

    Maze m;

    @Before
    public void setup() {
        m = new Maze();
        m.map[0][1].setType(TypeCell.EXIT);
    }

    @Test
    public void TestCopyThisCellState() {

        Maze test = new Maze();
        assertNotEquals(test.map[5][5].type, m.map[0][1].type);
        test.copyCell(new Coordinate(5, 5), m.map[0][1]);
        assertEquals(test.map[5][5].type, m.map[0][1].type);

    }

    @Test
    public void TestCanWalkOn() {
        Maze test1 = new Maze();
        test1.map[0][1].setType(TypeCell.EXIT);
        test1.map[0][2].setType(TypeCell.WALL);
        test1.map[0][0].setType(TypeCell.PATH);
        assertFalse(test1.canWalkOn(new Coordinate(0, 2)));// is a wall
        assertTrue(test1.canWalkOn(new Coordinate(0, 0)));// is a path
        assertTrue(test1.canWalkOn(new Coordinate(0, 1)));// is an exit
        assertFalse(test1.canWalkOn(new Coordinate(1, 0)));// is unknown

    }

    @Test
    public void TestInBounds() {
        assertEquals(7, m.getCol());
        assertEquals(7, m.getCol());
        assertTrue(m.inBounds(new Coordinate(0, 0)));
        assertTrue(m.inBounds(new Coordinate(0, 5)));
        assertFalse(m.inBounds(new Coordinate(50, 50)));
    }

}
