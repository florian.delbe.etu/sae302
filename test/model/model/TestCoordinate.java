package model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import main.model.Coordinate;

public class TestCoordinate {

    Coordinate coords;

    @Before
    public void setup() {
        coords = new Coordinate(5, 5);
    }

    @Test
    public void testStringToCoordinate() {
        Coordinate test = coords.stringToCoordinate("5,5");
        assertEquals(coords.getCol(), test.getCol());
        assertEquals(coords.getRow(), test.getRow());
    }
}
