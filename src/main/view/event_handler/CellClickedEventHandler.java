package main.view.event_handler;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import main.model.Observer;
import main.model.Subject;
import main.view.ViewCell;

public class CellClickedEventHandler implements EventHandler<MouseEvent>,Subject{
    ArrayList<Observer> observers;
    ViewCell viewCell;
    public CellClickedEventHandler(){
        this.observers=new ArrayList<>();
    }
    @Override
    public void handle(MouseEvent arg0) {
        
        notifyObservers(this.viewCell.coords);
    }

    @Override
    public void attach(Observer obs) {
        this.observers.add(obs);
       }

    @Override
    public void detach(Observer obs) {
        this.observers.remove(obs);
    }

    @Override
    public void notifyObservers() {
        for (Observer obs : observers) {
            obs.update();
        
    }}
    @Override
    public void notifyObservers(Object obj) {
        for (Observer obs : observers) {
            obs.update(obj);
        }
    }
    
}
