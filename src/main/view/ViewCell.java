package main.view;

import javafx.scene.control.TableCell;

import main.model.Coordinate;

public class ViewCell extends TableCell {
    public Coordinate coords;

    public ViewCell(){
        this.coords=new Coordinate(getIndex(), getIndex());
    }

}
