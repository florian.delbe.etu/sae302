package main.view;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import main.model.Game;
import main.model.Maze;
import main.model.TypeCell;

public class GameGrid extends Application {
    Game game;
    GridPane grid;

    @Override
    public void start(Stage arg0) throws Exception {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'start'");

    }

    public GridPane generateGridGame(int length, int height, Maze maze) {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < height; j++) {
                grid.add(generateButton(maze.map[i][j].getType()), i, j);
            }
        }
        return this.grid;
    }

    public Button generateButton(TypeCell type) {
        Button button = new Button();
        button.setPrefHeight(Region.USE_COMPUTED_SIZE);
        ImageView result = new ImageView("" + type);
        button.setGraphic(result);
        button.setContentDisplay(ContentDisplay.TOP);
        button.setPadding(Insets.EMPTY);
        /*
         * button.setOnAction(new EventHandler<ActionEvent>() {
         * public void handle(ActionEvent arg0) {
         * 
         * 
         * };
         * });
         */
        return button;
    }

}
