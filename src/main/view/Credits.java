package main.view;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Credits extends Application{

    @Override
    public void start(Stage arg0) throws Exception {
        Pane root =new Pane();
        root.setPrefSize(1050, 600);
        Text text= new Text("Made by Florian Delbe \n with help of:\nGwendal Margely(Maze generator and help fix code) \n Tanguy Sergeant(help fix technical issues)\n Antoine Hazebrouck (giving exemple of javafx code) \n and Internet");
        root.getChildren().add(text);
        root.setTranslateX(50);
        root.setTranslateY(200);
        Scene scene= new Scene(root);
        arg0.setTitle("Credits");
        arg0.setScene(scene);
        arg0.show();
    }
    

    public static void main(String[] args) {
        launch(args);
    }
}
