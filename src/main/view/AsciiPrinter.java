package main.view;

public class AsciiPrinter {
    public int height;
    public int length;
    private final char vide = '/';
    private final char wall = '#';
    private final char path = '.';
    private final char monster = 'M';
    //private final char hunter = 'X';
    private final char exit = 'Z';

    char unknown = ' ';

    public AsciiPrinter (int height, int length) {
        this.height = height;
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void printer (main.model.Cell[][] maze) {
        //Coordinate arrow = main.model.Hunter.getArrow();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length; j++) {
                /*if (i == arrow.getRow() && j == arrow.getCol()) {
                    System.out.print(hunter);
                }*/
                //implement later Arrow display
                switch (maze[i][j].getType()) {
                    case VOID -> System.out.print(vide);
                    case UNKNOWN -> System.out.print(unknown);
                    case WALL -> System.out.print(wall);
                    case PATH -> {
                        if (maze[i][j].isMonster()) {
                            System.out.print(monster);
                        } else if (maze[i][j].getTurn() != -1) {
                            System.out.print(Character.forDigit(maze[i][j].getTurn(),10));
                        } else {
                            System.out.print(path);
                        }
                    }
                    case EXIT -> System.out.print(exit);
                }
            }
        }
    }
}
