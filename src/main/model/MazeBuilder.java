package main.model;

import static main.model.Coordinate.between;
import static main.model.Coordinate.same;
import static main.model.Files.checkFile;
import static main.model.Files.checkSize;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class MazeBuilder {
    public Coordinate spawn;
    public Coordinate exit;
    private Coordinate grid;
    public int height;
    public int length;
    public Maze maze;
    public Level level;

    public MazeBuilder() {
    }
    public Maze generateMaze(String filepath) {
        if (checkFile(new File(filepath))) {
            Properties properties = new Properties();
            try (InputStream input = new FileInputStream("file:res/levels/" + filepath)) {
                // load a properties file
                properties.load(input);
                // get the property value and print it out
                this.height = Integer.parseInt(properties.getProperty("height"));
                this.length = Integer.parseInt(properties.getProperty("length"));
                this.maze = new Maze(height, length);
                //this.setMask(properties.getProperty("mask"));
                this.setSpawn();
                this.grid = new Coordinate();
                updateGrid();
                this.setExit();
                this.drawPath(this.exit);
                maze.map[exit.getRow()][exit.getCol()].setType(TypeCell.EXIT);
                this.maze.fill(TypeCell.UNKNOWN, TypeCell.WALL);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return maze;
    }
    public void drawPath(Coordinate current) {
        Random r = new Random();
        List<Coordinate> panel = new ArrayList<>();

        maze.getCell(current).setType(TypeCell.PATH);
        panel.add(new Coordinate(current.getRow(), current.getCol() - 2));
        panel.add(new Coordinate(current.getRow(), current.getCol() + 2));
        panel.add(new Coordinate(current.getRow() - 2, current.getCol()));
        panel.add(new Coordinate(current.getRow() + 2, current.getCol()));
        while (!panel.isEmpty()) {
            Coordinate test = panel.get(r.nextInt(panel.size()));
            if (maze.inBounds(test)) {
                if (maze.getCell(test).getType() == TypeCell.UNKNOWN) {
                    maze.getCell(between(current, test)).setType(TypeCell.PATH);
                    drawPath(test);
                }
                panel.remove(test);
            }
        }
    }

    public Maze generateMaze() {
        return generateMaze(RandomLevel());
    }
    public void setMask(String maskPath) {
        try (InputStream stream = new FileInputStream("file:res/masks/" + maskPath)){
            int buffer = -1;
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < length && ((buffer = stream.read()) != -1); j++) {
                    if (buffer == '\n') { buffer = stream.read(); }
                    this.maze = new Maze(height, length);
                    if (buffer == '1') {
                        maze.setCell(new Coordinate(i, j), TypeCell.VOID);
                    };
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * setup a Exit at random Coordinate
     */
    public void setSpawn() {
        Random r = new Random();
        this.spawn = new Coordinate();
        boolean condition = true;

        while (condition) {
            this.spawn.setRow(r.nextInt(this.height));
            this.spawn.setCol(r.nextInt(this.length));
            condition = (maze.getCell(spawn).getType() == TypeCell.UNKNOWN);
        }
    }
    public void setExit() {
        updateGrid();
        Random r = new Random();
        this.exit = new Coordinate();
        boolean condition = true;

        while (condition) {
            this.exit.setRow(r.nextInt((this.height - 1) / 2) * 2 + (1 - this.grid.getRow()));
            this.exit.setCol(r.nextInt((this.length - 1) / 2) * 2 + (1 - this.grid.getCol()));
            condition = (maze.getCell(exit).getType() == TypeCell.UNKNOWN) && !same(spawn, exit);
        }
        maze.map[exit.getRow()][exit.getCol()].setType(TypeCell.EXIT);
    }

    public void updateGrid() {
        this.grid.setRow((spawn.getRow() + 1) % 2);
        this.grid.setCol((spawn.getCol() + 1) % 2);
    }

    public Coordinate getGrid() {
        this.updateGrid();
        return this.grid;
    }

    public boolean isOnGrid(Coordinate test) {
        return ((test.getRow() % 2) == grid.getRow()) && ((test.getCol() % 2) == (grid.getCol()));
    }

    /**
     * check if a Coords is on a path by checking if the maze is on a row or col
     * even
     * 
     * @param test the Coordinate you check
     * @return if its on a even col and row
     */
    public boolean isOnPath(Coordinate test) {
        return ((test.getRow() % 2) != grid.getRow()) && ((test.getCol() % 2) != (grid.getCol()));
    }

    /**
     * generate a Maze
     * 
     * @return a fresh new maze
     */

    /**
     * load a random level
     */
    public void loadLevel() {
        this.loadLevel(RandomLevel());
    }

    /**
     * generate a new level (a imported maze)
     *
     * @param path path to the file
     */
    public void loadLevel(String path) {
        try {
            this.level = new Level(path);
            this.maze.setMap(level.getMask());
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public static String RandomLevel() {
        // Modify later for a real randomizer
        return "default.properties";
    }

}
