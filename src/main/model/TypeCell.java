package main.model;

public enum TypeCell {
    VOID, WALL, PATH, EXIT, UNKNOWN
}
