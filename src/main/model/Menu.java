package main.model;

public class Menu {
    public int choices;

    public Menu(int choices) {
        this.choices = choices;
    }

    public int getChoices() {
        return choices;
    }

    public void setChoices(int choices) {
        this.choices = choices;
    }

    public int chose(int x) {
        //Player chose between the &choices& choices and returns choice's number
        return x;
    }
}
