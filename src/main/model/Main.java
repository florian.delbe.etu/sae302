package main.model;

import main.view.Menu;

public class Main {
    public static Game game;
    // public HunterView hunterview;
    // public MonsterView monsterView;

    public static void main(String[] argv) {
        game = new Game();
        Menu.launch(argv);
        game.play();
    }
}
