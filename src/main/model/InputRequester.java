package main.model;

import java.util.Scanner;

public class InputRequester {
    static String input;

    public static String giveInput() {
        Scanner sc = new Scanner(System.in);
        input = sc.next();
        return input;

    }
}
