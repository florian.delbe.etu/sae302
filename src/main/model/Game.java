package main.model;

import java.util.Random;

public class Game {
    public Hunter hunter;
    public Monster monster;
    public Player player;
    public Maze maze;

    public int actualTurn;
    public static boolean win;

    public Game() {
        this.maze = new Maze();
        this.actualTurn = 0;
        this.hunter = new Hunter(this.maze);
        this.monster = new Monster();
        win = false;
        setup();
    }

    public void setup() {
        MazeBuilder builder = new MazeBuilder();
        System.out.println("test");
        this.maze = builder.generateMaze();
    }

    public void play() {
        Menu startMenu = new Menu(2);
        Round round;
        startMenu.chose(1);
        System.out.println(this.maze.exit);
        while (!monster.hasWin(maze.exit) || !hunter.hasWin(maze, monster.position)) {
            round = new Round(monster);
            round.play(maze);
            this.maze = round.player.view;
            round = new Round(hunter);
            round.play(maze);
            this.maze = round.player.view;
            System.out.println(hunter.view.exit);
            System.out.println(monster.view.exit);
        }

    }

}