package main.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Player implements Subject {
    private static int number = 0;
    public int no;
    public int points;
    public String name;

    public Maze view;
    protected List<Observer> observers;

    public Player() {
        number += 1;
        this.no = number;
        this.points = 0;
        this.view = null;
        observers = new ArrayList<>();
    }

    public static int getNumber() {
        return number;
    }

    public int getNo() {
        return no;
    }

    public int getPoints() {
        return points;
    }

    public Maze getView() {
        return view;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public void setView(Maze view) {
        this.view = view;
    }

    public abstract boolean playTurn(Maze maze, Coordinate coords);

}