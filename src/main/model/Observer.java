package main.model;

public interface Observer {
    public void update();

    public void update(Object obj);
}