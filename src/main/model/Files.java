package main.model;

import java.io.File;

public class Files {
    public static boolean checkFile(File file) {
        if (!file.exists())
            return false;
        if (!file.isFile())
            return false;
        return file.canRead();
    }

    public static boolean checkSize(File file, long size) {
        if (checkFile(file)) {
            return (file.length() > size);
        } else {
            return false;
        }
    }
}
