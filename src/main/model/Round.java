package main.model;

public class Round {
    public static int number = 0;
    public Player player;

    public Round (Player player) {
        this.player = player;
        number++;
    }

    public int play (Maze maze) {
        Coordinate coords= new Coordinate(-1, -1);
        while (!maze.inBounds(coords)) {
            coords= coords.stringToCoordinate(InputRequester.giveInput());
            player.playTurn(maze, coords);
        }

        System.out.println(number);
        return 0;
    }
}
