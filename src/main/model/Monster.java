package main.model;

public class Monster extends Player {
    public Coordinate position;
    public int type;

    /**
     * Constructor for Monster
     * 
     * @param position Coordinate of the monster
     */
    public Monster(Coordinate position) {
        this(position, null);
        // ask
    }

    public Monster() {
        this(new Coordinate(0, 0));
    }

    public Monster(Coordinate position, Maze maze) {
        setView(maze);
        this.position = position;
        this.type = 1;
    }

    public Monster(Maze maze) {
        this(new Coordinate(0, 0), maze);
    }

    public void setView(Maze view) {
        super.view = view;
    }

    /**
     * check if the monster can move on this coords of the maze
     * 
     * @param coords coords you want to move on
     * @param maze   maze you're playing on
     * @return if you have mooved or not
     */
    public boolean move(Coordinate coords, Maze maze) {
        boolean check = false;
        if (this.position.inRange(coords, 1) && maze.canWalkOn(coords)) {
            maze.getMap()[coords.getRow()][coords.getCol()].setMonster(true);
            maze.getMap()[position.getRow()][position.getCol()].setMonster(false);
            this.position = coords;
            check = true;
        }
        return check;
    }

    /**
     * 
     * 
     * @param winCondition the coordinate of the win condition
     * @return if you have winned or not
     */
    public boolean hasWin(Coordinate winCondition) {
        return this.position.getRow() == winCondition.getRow() && this.position.getCol() == winCondition.getCol();
    }

    @Override
    public boolean playTurn(Maze maze, Coordinate coords) {
        boolean result = false;
        if (coords.inRange(this.position, 1)) {
            move(coords, maze);
            notifyObservers();
            if (this.hasWin(coords)) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public void attach(Observer obs) {
        super.observers.add(obs);
    }

    @Override
    public void detach(Observer obs) {
        super.observers.remove(obs);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    @Override
    public void notifyObservers(Object obj) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'notifyObservers'");
    }

}
