package main.model;

import java.util.Random;

public class Maze {
    public Cell[][] map;
    public int row;
    public int col;
    public Coordinate spawn;
    public Coordinate exit;

    public String background;

    /**
     * Constructor for maze, initalize the cell of the maze
     * 
     * @param row   the row of the maze
     * @param col the col of the maze
     */
    public Maze(int row, int col) {
        this.row = (row > 0 ? row : (-row + (row == 0 ? 1 : 0)));
        this.col = (col > 0 ? col : (-col + (col == 0 ? 1 : 0)));
        this.spawn = new Coordinate(-1,-1);
        this.exit = new Coordinate(-1,-1);
        this.emptyMap();
        // The two previous lines disables creating a maze with negative sizes and
        // replace 0 sizes by 1.
    }

    public Maze() {
        this(7, 7);
    }

    // Getters
    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public Coordinate getSpawn() {
        return spawn;
    }

    public Cell[][] getMap() {
        return map;
    }

    public Cell getCell(Coordinate location) {
        return map[location.getRow()][location.getCol()];
    }

    // Setters
    public void setCol(int col) {
        this.col = col;
        this.emptyMap();
    }

    public void setRow(int row) {
        this.row = row;
        this.emptyMap();
    }

    public void emptyMap() {
        this.map = new Cell[this.col][this.row];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map.length; j++) {
                this.map[i][j] = new Cell();
            }
        }
    }

    public void setSpawn(Coordinate spawn) {
        this.spawn = spawn;
    }

    public void setCell(Coordinate location) {
        this.setCell(location, TypeCell.UNKNOWN);
    }
    public void setCell(Coordinate location, TypeCell type) {
        this.map[location.getRow()][location.getCol()] = new Cell();
        this.map[location.getRow()][location.getCol()].setType(type);
    }

    public void setMap(Cell[][] map) {
        this.map = map;
    }

    // Methods

    /**
     * Create a random Spawn point in the maze
     * 
     * @return the Coordinate of the new spawn
     */
    public Coordinate randomSpawn() {
        this.spawn = new Coordinate(new Random().nextInt(row), new Random().nextInt(col));
        return spawn;
    }

    /**
     * Copy a cell and replace the previous one
     * 
     * @param location location of the old cell
     * @param copyCat  the cell you want to copy
     * @see Cell
     */
    public void copyCell(Coordinate location, Cell copyCat) {
        this.map[location.getRow()][location.getCol()].copyCell(copyCat);
    }

    /**
     * Test if the monster can walk on this cell
     * 
     * @param position the Coordinate where you want to walk
     * @return if the monster can walk on this cell
     */
    public boolean canWalkOn(Coordinate position) {
        boolean result = false;
        if (inBounds(position)) {
            Cell tmp = map[position.getRow()][position.getCol()];
            result = ((tmp.getType() == TypeCell.PATH) || (tmp.getType() == TypeCell.EXIT));
        }
        return result;
    }

    /**
     * check if the Coordinate is in bounds of the maze
     * 
     * @param position the position you want to check
     * @return if the Coordinate is in bounds of the maze
     */
    public boolean inBounds(Coordinate position) {
        int row = position.getRow();
        int col = position.getCol();
        return (row >= 0 && row < this.row) && (col >= 0 && col < this.col);
    }

    public void setBackground(String path) {
        this.background = "res/images/levels/" + path;
    }
    public String getBackground() {
        return background;
    }

    public void fill(TypeCell target, TypeCell replace) {
        for (Cell[] cells : map) {
            for (int j = 0; j < map.length; j++) {
                cells[j].setType(cells[j].getType() == target ? replace : target);
            }
        }
    }

    public String toString() {
        return main.ascii.Maze.toString(this);
    }
}