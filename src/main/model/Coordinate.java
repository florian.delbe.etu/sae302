package main.model;

import fr.univlille.iutinfo.cam.player.perception.ICoordinate;

public class Coordinate implements ICoordinate {
    private int col;
    private int row;

    public Coordinate(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Coordinate() {
        this(0, 0);
    }

    @Override
    public int getRow() {
        return row;
    }

    @Override
    public int getCol() {
        return col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    /**
     * Check if your Coords are in range of the other Coords gived( range gived by
     * range)
     * 
     * @param coords the coords you want to check in range
     * @param range  the range you have
     * @return if the gived cell is in range
     */
    public boolean inRange(Coordinate coords, int range) {
        boolean isInRange = false;
        if (this.getRow() == coords.getRow()) {
            isInRange = this.getCol() == coords.getCol() - range || this.getCol() == coords.getCol() + range;
        } else if (this.getCol() == coords.getCol()) {
            isInRange = this.getRow() == coords.getRow() - range || this.getRow() == coords.getRow() + range;
        }
        return isInRange;
    }

    /**
     * check if the 2 Coordinate gived are the sames
     * 
     * @param c1 first Coordinate
     * @param c2 seconds Coordinate
     * @return if they are the same
     */
    public static boolean same(Coordinate c1, Coordinate c2) {
        return ((c1.getRow() == c2.getRow()) && (c1.getCol() == c2.getCol()));
    }

    /**
     * Create a new Coordinate from a String
     * 
     * @param input the input String
     * @return a fresh new Coordinate class
     */
    public Coordinate stringToCoordinate(String input) {
        Coordinate res = new Coordinate();
        String[] temp = input.split(",");
        res.setRow(Integer.parseInt(temp[0]));
        res.setCol(Integer.parseInt(temp[1]));
        return res;

    }

    public static Coordinate between(Coordinate c1, Coordinate c2) {
        Coordinate result = new Coordinate();
        result.setRow((c1.getRow() + c2.getRow()) / 2);
        result.setCol((c1.getCol() + c2.getCol()) / 2);
        return result;
    }

    @Override
    public String toString() {
        return "" + this.row + " " + this.col;
    }
}