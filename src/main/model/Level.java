package main.model;

import java.io.*;
import java.util.Properties;

import static main.model.Files.checkFile;
import static main.model.Files.checkSize;

public class Level {
    public Properties properties;
    public String title;
    public int height;
    public int length;
    public Cell[][] mask;
    public String background;

    public Level (String path) throws FileNotFoundException {
        File levelFile = new File(path);
        if (!checkFile(levelFile)) {
            throw new FileNotFoundException("The path is either not related to a file or the file isn't readable");
        }
        this.loadProperties(levelFile);
        this.title = properties.getProperty("title");
        this.height = Integer.parseInt(this.properties.getProperty("height"));
        this.length = Integer.parseInt(this.properties.getProperty("length"));
        loadMask(new File(properties.getProperty("mask")));
        this.background = properties.getProperty("background");
    }

    public void loadProperties(File levelFile) {
        properties = new Properties();
        try (InputStream source = new FileInputStream(levelFile)) {
            properties.load(source);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Cell[][] getMask() {
        return mask;
    }

    public void loadMask(File maskFile) throws FileNotFoundException {
        if (!checkSize(maskFile, (long) this.height * this.length)) {
            throw new FileNotFoundException("The path is either not related to a file or the file isn't readable");
        }
        try (InputStream inputStream = new FileInputStream(maskFile)) {
            int byteRead = -1;
            for (int i = 0; i < height && ((byteRead = inputStream.read()) != -1); i++) {
                for (int j = 0; j < length &&  (byteRead != '\n'); j++) {
                    mask[i][j].setType((((char) byteRead) != '0' ? TypeCell.VOID : TypeCell.UNKNOWN));
                }
            }
            inputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
