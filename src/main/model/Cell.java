package main.model;

public class Cell {

    public TypeCell type;
    private int turn;

    public boolean monster;
    public Coordinate previous;
    public boolean hasBeenShot;

    /**
     * Constructor of Monster, all options
     * 
     * @param type     type of your cell
     * @param previous Coordinate of the previous cell you were
     */
    public Cell(TypeCell type, Coordinate previous) {
        this.type = type;
        this.previous = previous;
        this.turn = -1;
    }

    public Cell(TypeCell type) {
        this(type, null);
    }

    public Cell() {
        this(TypeCell.UNKNOWN);
    }

    // getters
    public TypeCell getType() {
        return type;
    }

    public int getTurn() {
        return turn;
    }

    public boolean isMonster() {
        return monster;
    }

    public Coordinate getPrevious() {
        return previous;
    }

    // setters
    /**
     * Copy a Cell and replace this one by the copied cell
     * 
     * @param copyCat the cell you copy
     */
    public void copyCell(Cell copyCat) {
        this.type = copyCat.getType();
        this.turn = copyCat.getTurn();
        this.monster = copyCat.isMonster();
        this.previous = getPrevious();
    }

    public void setType(TypeCell type) {
        this.type = type;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setMonster(boolean monster) {
        this.monster = monster;
    }

    public void setPrevious(Coordinate previous) {
        this.previous = previous;
    }

    // Methods

}
