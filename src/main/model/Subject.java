package main.model;

public interface Subject {
    public void attach(Observer obs);

    public void detach(Observer obs);

    public void notifyObservers();
    public void notifyObservers(Object obj);

}
