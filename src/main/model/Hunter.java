package main.model;

public class Hunter extends Player {
    private final Coordinate arrow;

    public Hunter() {
        this(null);
    }

    public Hunter(Maze view) {
        super();
        this.arrow = new Coordinate();
        this.setView(view);
    }

    public int getRow() {
        return arrow.getRow();
    }

    public int getCol() {
        return arrow.getCol();
    }

    public Coordinate getArrow() {
        return arrow;
    }

    public void setView(Maze view) {
        super.view = view;
    }

    public boolean shot(Coordinate coords) {
        boolean check = false;
        if (!(coords.getCol() < 0 || coords.getCol() > view.getCol() || coords.getRow() < 0
                || coords.getRow() > view.getRow())) {
        }
        return check;
    }

    private boolean shotOnMonster(Maze maze, Coordinate coords) {
        return maze.getCell(coords).monster;
    }

    public boolean hasWin(Maze maze, Coordinate winCondition) {
        return shotOnMonster(maze, winCondition);
    }

    @Override
    public boolean playTurn(Maze maze, Coordinate coords) {
        boolean result = false;
        if (shot(coords)) {
            shotOnMonster(maze, coords);
            notifyObservers();
            result = true;
        }
        return result;

    }

    @Override
    public void attach(Observer obs) {
        super.observers.add(obs);
    }

    @Override
    public void detach(Observer obs) {
        super.observers.remove(obs);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    @Override
    public void notifyObservers(Object obj) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'notifyObservers'");
    }

}
