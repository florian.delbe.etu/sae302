package main.ascii;

import main.model.Cell;
import main.model.Coordinate;

import static jdk.internal.joptsimple.internal.Strings.repeat;

public class Maze {
    public static int height;
    public static int length;
    private static final char vide = '/';
    private static final char wall = '#';
    private static final char path = '.';
    private static final char monster = 'M';
    private static final char hunter = 'X';
    private static final char exit = 'Z';
    private static final char spawn = 'A';
    private static Coordinate arrow;

    static char unknown = ' ';

    public Maze(int height, int length) {
        Maze.height = height;
        Maze.length = length;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public void setHeight(int height) {
        Maze.height = height;
    }

    public void setLength(int length) {
        Maze.length = length;
    }

    public static String toString(main.model.Maze maze) {
        //Coordinate arrow = main.model.Hunter.getArrow();
        Cell[][] map = maze.map;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length; j++) {
                switch (map[i][j].getType()) {
                    case VOID -> result.append(repeat(vide, 2));
                    case UNKNOWN -> result.append(repeat(unknown, 2));
                    case WALL -> result.append(repeat(wall, 2));
                    case PATH -> {
                        if (map[i][j].isMonster()) {
                            result.append(repeat(monster, 2));
                        } else if (map[i][j].getTurn() != -1) {
                            result.append(Character.forDigit(map[i][j].getTurn(), 99));
                        } else if (maze.getSpawn().equals(new Coordinate(i, j))) {
                            result.append(repeat(spawn, 2));
                        } else {
                            result.append(repeat(path, 2));
                        }
                    }
                    case EXIT -> result.append(repeat(exit, 2));
                }
            }
            result.append('\n');
        }
        return result.toString();
    }
    public static void setArrow(Coordinate arrow) {
        Maze.arrow = arrow;
    }

    public static Coordinate getArrow() {
        return arrow;
    }
}
