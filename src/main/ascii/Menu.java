package main.ascii;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.List;

public class Menu {
    public List<String> choices;
    public String border;
    public int index;
    public Menu(List<String> choices) {
        this.choices = choices;
        this.border = "oooo-|";
        this.index = 0;
    }

    public int VerticalChoice() {
        printMenu();
        try {
            int answer = Integer.parseInt(String.valueOf(System.in.read()));
            if (answer >= 0 && answer < choices.size()) {
                this.index = answer;
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }

        return this.index;
    }

    public void printMenu() {
        int lScreen = Printer.length;
        String[] menu = new String[choices.size() + 4];
        int menuLength = 6;
        for (String choice : choices) {
            if (choice.length() <= lScreen - 6) {
                menuLength = (Math.max(choice.length(), menuLength));
            }
        }
        menu[0] = border.charAt(0) + ("" + border.charAt(4)).repeat(menuLength + 2) + border.charAt(1);
        menu[1] = border.charAt(5) + (" ").repeat(menuLength + 2) + border.charAt(5);
        for (int i = 0; i < choices.size() && i < lScreen - 6; i++) {
            menu[i] = border.charAt(5) + i + (index == i ? "\033[1;91m" : "\033[0;37m");
            menu[i] += choices.get(i).substring(0, lScreen - 6) ;
            menu[i] += (index == i ? "\033[1;91m" : "\033[0;37m") + " " + border.charAt(5);
        }
        menu[choices.size() + 2] = border.charAt(0) + ("" + border.charAt(4)).repeat(menuLength + 2) + border.charAt(1);
        menu[choices.size() + 3] = border.charAt(5) + (" ").repeat(menuLength + 2) + border.charAt(5);
        Printer.center(menu);
    }

    public void setBorder(String border) {
        this.border = border;
    }

    public void setBorder(char border) {
        this.border = ("" + border).repeat(6);
    }

    public void setBorder(char corner, char horizontal, char vertical) {
        this.border = ("" + corner).repeat(4) + horizontal + vertical;
    }

    public void setBorder(String corner, char horizontal, char vertical) {
        this.border = corner + horizontal + vertical;
    }
}
