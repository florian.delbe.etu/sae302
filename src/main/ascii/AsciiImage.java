package main.ascii;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;

public class AsciiImage {

    public String[] convertToAscii(String imagePath, int height, int length) {
        String[] result = new String[height];
        double[][] valueTab = new double[height][length];
        double hAccuracy;
        double wAccuracy;
        try {
            BufferedImage img = ImageIO.read(new File(imagePath));
            hAccuracy = (double) img.getHeight() / height;
            wAccuracy = (double) img.getWidth() / length;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    Color pixcol = new Color(img.getRGB(j, i));
                    double pixval = (((pixcol.getRed() * 0.30) + (pixcol.getBlue() * 0.59) + (pixcol
                            .getGreen() * 0.11)));
                    valueTab[i / height][j / length] += pixval / (hAccuracy * wAccuracy);
                }
            }
        } catch (IOException e) {
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < length; j++) {
                    valueTab[i / height][j / length] = 0;
                }
            }
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length; j++) {
                result[i] += strChar(valueTab[i][j]);
            }
        }
        return result;
    }

    public String strChar(double g) {
        String str = " ";
        if (g >= 240) {
            str = " ";
        } else if (g >= 210) {
            str = ".";
        } else if (g >= 190) {
            str = "*";
        } else if (g >= 170) {
            str = "+";
        } else if (g >= 120) {
            str = "^";
        } else if (g >= 110) {
            str = "&";
        } else if (g >= 80) {
            str = "8";
        } else if (g >= 60) {
            str = "#";
        } else {
            str = "@";
        }
        return str;
    }
}