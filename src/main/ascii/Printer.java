package main.ascii;

public class Printer {
    public static int height;
    public static int length;

    public static String[] screen;

    public void print(String[] screen) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length; j++) {
                System.out.print(screen[i].charAt(j) == '~' ? Printer.screen[i].charAt(j) : screen[i].charAt(j));
            }
        }
    }



    public static void center(String[] content) {
        int upMargin = (Printer.height - content.length) / 2;
        int leftMargin = (Printer.length - content[0].length()) / 2;
        for (int i = upMargin; i < height && content.length < (i + leftMargin); i++) {
            for (int j = leftMargin; j < length && content[i].length() < (j + leftMargin); j++) {
                char c = content[i - upMargin].charAt(j - leftMargin);
                if (c != '~') {
                    Printer.screen[i] = Printer.screen[i].substring(0, j) + c
                            + Printer.screen[i].substring(j + 1);
                }
            }
        }
    }

    public void print() {
        print(Printer.screen);
    }

    public void emptyScreen() {
        for (int i = 0; i < height; i++) {
            Printer.screen[i] = (" ").repeat(length);
        }
    }

    public static void setSize(int height, int length) {
        Printer.height = height;
        Printer.length = length;
    }

    public static String[] getScreen() {
        return Printer.screen;
    }

    public static void setScreen(String[] screen) {
        Printer.screen = screen;
        for (int i = 0; i < Printer.height; i++) {
            for (int j = 0; j < Printer.length; j++) {
                if (screen[i].charAt(j) != '~') {
                    Printer.screen[i] = Printer.screen[i].substring(0, j) + screen[i].charAt(j)
                            + Printer.screen[i].substring(j + 1);
                }
            }
        }
    }
}
