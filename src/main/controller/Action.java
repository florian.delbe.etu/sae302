package main.controller;

public abstract class Action {
    public Action () {
    }

    public abstract Object waitFor();

}
